package com.sergiovera.gui;


import com.github.lgooddatepicker.components.DatePicker;
import com.sergiovera.videoclub.Videoclub;

import javax.swing.*;

public class Ventana {

    private JPanel panel1;
    public JFrame frame;
    public JRadioButton peliculaRadioButton;
    public JRadioButton VideoJuegoRadioButton;
    public JLabel Nombre;
    public JLabel Codigo;
    public JLabel Estudio;
    public JTextField nombreTxt;
    public JTextField codigoTxt;
    public JTextField estudioTxt;
    public JLabel fechaEstreno;
    public DatePicker fechaEstrenoDPicker;
    public JTextField precioDirectorTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public JLabel directorPrecioLbl;
    public JButton borrarBtn;

    public DefaultListModel<Videoclub> dlmVideoclub;

    public Ventana() {
        frame = new JFrame("Videoclub");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmVideoclub=new DefaultListModel<Videoclub>();
        list1.setModel(dlmVideoclub);
    }

}
