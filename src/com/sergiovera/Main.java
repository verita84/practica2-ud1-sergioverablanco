package com.sergiovera;

import com.sergiovera.gui.Ventana;
import com.sergiovera.gui.VideoclubControlador;
import com.sergiovera.gui.VideoclubModelo;

public class Main {
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        VideoclubModelo modelo = new VideoclubModelo();
        VideoclubControlador controlador = new VideoclubControlador(vista, modelo);
    }
}
