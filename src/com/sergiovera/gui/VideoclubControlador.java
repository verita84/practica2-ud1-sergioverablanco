package com.sergiovera.gui;

import com.sergiovera.videoclub.Pelicula;
import com.sergiovera.videoclub.VideoJuego;
import com.sergiovera.videoclub.Videoclub;
import org.xml.sax.SAXException;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;


public class VideoclubControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private VideoclubModelo modelo;
    private File ultimaRutaExportada;

    public VideoclubControlador(Ventana vista, VideoclubModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);
    }


    /**
     * @param e Aqui enlaza los botones de nuevo, importar y exportar del menu con el codigo correspondiente para su funcionamiento
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Nombre\nCodigo\nEstudio\nFecha" +
                            vista.directorPrecioLbl.getText());
                    break;
                }

                if (modelo.existeNombre(vista.nombreTxt.getText())) {
                    Util.mensajeError("Ya existe un producto con este nombre\n+" +
                            vista.nombreTxt.getText());
                    break;
                }
                if (vista.peliculaRadioButton.isSelected()) {
                    modelo.altaPelicula(vista.nombreTxt.getText(), vista.codigoTxt.getText(), vista.estudioTxt.getText(),
                            vista.fechaEstrenoDPicker.getDate(), vista.precioDirectorTxt.getText());
                } else {
                    modelo.altaVideoJuego(vista.nombreTxt.getText(), vista.codigoTxt.getText(), vista.estudioTxt.getText(),
                            vista.fechaEstrenoDPicker.getDate(), (int) Double.parseDouble(vista.precioDirectorTxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Videojuego":
                vista.directorPrecioLbl.setText("Precio");
                break;
            case "Pelicula":
                vista.directorPrecioLbl.setText("Director");
                break;
            case "Borrar":
                limpiarCampos();
                refrescar();
                break;

        }
    }

    /**
     * @return Te avisa si hay campos vacios y salta un error
     */
    private boolean hayCamposVacios() {
        if (vista.precioDirectorTxt.getText().isEmpty() ||
                vista.nombreTxt.getText().isEmpty() ||
                vista.codigoTxt.getText().isEmpty() ||
                vista.estudioTxt.getText().isEmpty() ||
                vista.fechaEstrenoDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Aqui cada vez que añadimos un tipo de producto del videoclub, se borran los campos de los JTextField para añadir si queremos mas productos
     */
    private void limpiarCampos() {
        vista.precioDirectorTxt.setText(null);
        vista.nombreTxt.setText(null);
        vista.codigoTxt.setText(null);
        vista.estudioTxt.setText(null);
        vista.fechaEstrenoDPicker.setText(null);
        vista.nombreTxt.requestFocus();
    }

    /**
     *Cada vez que demos de alta un producto se refresca para la visualizacion de los datos
     */
    private void refrescar() {
        vista.dlmVideoclub.clear();
        for (Videoclub unVideoclub : modelo.obtenerVideoclub()) {
            vista.dlmVideoclub.addElement(unVideoclub);
        }
    }

    /**
     * @param listener Enlaza los botones con el codigo que tiene que realizar cada uno.
     */
    private void addActionListener(ActionListener listener) {
        vista.VideoJuegoRadioButton.addActionListener(listener);
        vista.peliculaRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoBtn.addActionListener(listener);
        vista.borrarBtn.addActionListener(listener);
    }
    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("videoclub.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }


    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * @throws IOException Archivo en el que podemos guardar diferentes propiedades de
     *                     configuración que la aplicación pueda leer y editar
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("videoclub.conf"), "Datos videoclub");
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Videoclub seleccionado = (Videoclub) vista.list1.getSelectedValue();
            vista.nombreTxt.setText((seleccionado.getNombre()));
            vista.codigoTxt.setText(seleccionado.getCodigo());
            vista.estudioTxt.setText(seleccionado.getEstudio());
            vista.fechaEstrenoDPicker.setDate(seleccionado.getFechaEstreno());

            if (seleccionado instanceof Pelicula) {
                vista.peliculaRadioButton.doClick();
                vista.precioDirectorTxt.setText(String.valueOf(((Pelicula) seleccionado).getDirector()));
            } else {
                vista.VideoJuegoRadioButton.doClick();
                vista.precioDirectorTxt.setText(String.valueOf(((VideoJuego) seleccionado).getPrecio()));
            }
        }
    }

    /**
     * @param e Al cerrar la ventana te pregunta si quieres cerrar la ventana o no
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

    }
    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }


}
