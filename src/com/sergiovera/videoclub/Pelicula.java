package com.sergiovera.videoclub;

import java.time.LocalDate;

public class Pelicula extends Videoclub {
    private String director;

    public Pelicula(){
        super();
    }
    public Pelicula(String nombre, String codigo, String estudio, LocalDate fechaEstreno, String director) {
        super(nombre, codigo, estudio, fechaEstreno);
        this.director = director;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "Pelicula{" +
                "nombre='" + nombre + '\'' +
                ", codigo='" + codigo + '\'' +
                ", id='" + estudio + '\'' +
                ", fechaEstreno=" + fechaEstreno + '\'' +
                ", director=" + director +
                '}';
    }
}
