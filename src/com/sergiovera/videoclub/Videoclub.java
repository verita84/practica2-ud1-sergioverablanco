package com.sergiovera.videoclub;

import java.time.LocalDate;

public abstract class Videoclub {
    public String nombre;
    public String codigo;
    public String estudio;
    public LocalDate fechaEstreno;

    public Videoclub(String nombre, String codigo, String estudio, LocalDate fechaEstreno) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.estudio = estudio;
        this.fechaEstreno = fechaEstreno;
    }

    public Videoclub() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEstudio() {
        return estudio;
    }

    public void setEstudio(String estudio) {
        this.estudio = estudio;
    }

    public LocalDate getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(LocalDate fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    @Override
    public String toString() {
        return "Videoclub{" +
                "nombre='" + nombre + '\'' +
                ", codigo='" + codigo + '\'' +
                ", id='" + estudio + '\'' +
                ", fechaEstreno=" + fechaEstreno +
                '}';
    }
}
