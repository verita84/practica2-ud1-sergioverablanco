package com.sergiovera.videoclub;

import java.time.LocalDate;

public class VideoJuego extends Videoclub{
    private int precio;

    public VideoJuego(){
        super();
    }

    public VideoJuego(String nombre, String codigo, String estudio, LocalDate fechaEstreno, int precio) {
        super(nombre, codigo, estudio, fechaEstreno);
        this.precio = precio;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Videojuego{" +
                "nombre='" + nombre + '\'' +
                ", codigo='" + codigo + '\'' +
                ", id='" + estudio + '\'' +
                ", fechaEstreno=" + fechaEstreno + '\'' +
                ", precio=" + precio +
                '}';
    }
}
