package com.sergiovera.gui;

import com.sergiovera.videoclub.Pelicula;
import com.sergiovera.videoclub.VideoJuego;
import com.sergiovera.videoclub.Videoclub;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class VideoclubModelo {
    private ArrayList<Videoclub> listaVideoclub;

    public VideoclubModelo(){
        listaVideoclub = new ArrayList<Videoclub>();
    }

    public ArrayList<Videoclub> obtenerVideoclub() {
        return listaVideoclub;
    }

    /**
     * @param nombre Damos de alta un Videojuego
     * @param codigo
     * @param estudio
     * @param fechaEstreno
     * @param precio
     */
    public void altaVideoJuego(String nombre, String codigo, String estudio, LocalDate fechaEstreno, int precio) {
        VideoJuego nuevoVideojuego=new VideoJuego(nombre,codigo, estudio,fechaEstreno,precio);
        listaVideoclub.add(nuevoVideojuego);
    }

    /**
     * @param nombre Damos de alta una película
     * @param codigo
     * @param estudio
     * @param fechaEstreno
     * @param director
     */
    public void altaPelicula(String nombre, String codigo, String estudio, LocalDate fechaEstreno, String director) {
        Pelicula nuevaPelicula=new Pelicula(nombre,codigo, estudio,fechaEstreno,director);
        listaVideoclub.add(nuevaPelicula);
    }

    /**
     * @param nombre Comprobamos si ya existe un nombre en la lista
     * @return
     */
    public boolean existeNombre(String nombre) {
        for (Videoclub unVideoClub:listaVideoclub) {
            if(unVideoClub.getNombre().equals(nombre)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param fichero Codigo para exportarXML
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Videoclub");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoVideoclub = null, nodoDatos = null;
        Text texto = null;

        for (Videoclub unVideoClub : listaVideoclub) {

            /*Añado dentro de la etiqueta raiz (Videoclub) una etiqueta
            dependiendo del tipo de producto que este almacenando
            (pelicula o videojuego)
             */
            if (unVideoClub instanceof VideoJuego) {
                nodoVideoclub = documento.createElement("VideoJuego");

            } else {
                nodoVideoclub = documento.createElement("Pelicula");
            }
            raiz.appendChild(nodoVideoclub);

            /*Dentro de la etiqueta videoclub le añado
            las subetiquetas con los datos de sus
            atributos (nombre, codigo, etc)
             */
            nodoDatos = documento.createElement("nombre");
            nodoVideoclub.appendChild(nodoDatos);

            texto = documento.createTextNode(unVideoClub.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("codigo");
            nodoVideoclub.appendChild(nodoDatos);

            texto = documento.createTextNode(unVideoClub.getCodigo());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("estudio");
            nodoVideoclub.appendChild(nodoDatos);

            texto = documento.createTextNode(unVideoClub.getEstudio());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-estreno");
            nodoVideoclub.appendChild(nodoDatos);

            texto = documento.createTextNode(unVideoClub.getFechaEstreno().toString());
            nodoDatos.appendChild(texto);

            /* Como hay un dato que depende del tipo del videoclub
            debo acceder a él controlando el tipo de objeto
             */
            if (unVideoClub instanceof VideoJuego) {
                nodoDatos = documento.createElement("precio");
                nodoVideoclub.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((VideoJuego) unVideoClub).getPrecio()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("director");
                nodoVideoclub.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Pelicula) unVideoClub).getDirector()));
                nodoDatos.appendChild(texto);
            }

        }
        /*
        Guardo los datos en un "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    /**
     * @param fichero Codigo para importarXML
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaVideoclub = new ArrayList<Videoclub>();
        VideoJuego nuevoVideojuego = null;
        Pelicula nuevaPelicula = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoVideoclub = (Element) listaElementos.item(i);


            if (nodoVideoclub.getTagName().equals("Videojuego")) {
                nuevoVideojuego = new VideoJuego();
                nuevoVideojuego.setNombre(nodoVideoclub.getChildNodes().item(0).getTextContent());
                nuevoVideojuego.setCodigo(nodoVideoclub.getChildNodes().item(1).getTextContent());
                nuevoVideojuego.setEstudio(nodoVideoclub.getChildNodes().item(2).getTextContent());
                nuevoVideojuego.setFechaEstreno(LocalDate.parse(nodoVideoclub.getChildNodes().item(3).getTextContent()));
                nuevoVideojuego.setPrecio(Integer.parseInt(nodoVideoclub.getChildNodes().item(4).getTextContent()));

                listaVideoclub.add(nuevoVideojuego);
            } else {
                if (nodoVideoclub.getTagName().equals("Pelicula")) {
                    nuevaPelicula = new Pelicula();
                    nuevaPelicula.setNombre(nodoVideoclub.getChildNodes().item(0).getTextContent());
                    nuevaPelicula.setCodigo(nodoVideoclub.getChildNodes().item(1).getTextContent());
                    nuevaPelicula.setEstudio(nodoVideoclub.getChildNodes().item(2).getTextContent());
                    nuevaPelicula.setFechaEstreno(LocalDate.parse(nodoVideoclub.getChildNodes().item(3).getTextContent()));
                    nuevaPelicula.setDirector(nodoVideoclub.getChildNodes().item(4).getTextContent());

                    listaVideoclub.add(nuevaPelicula);
                }
            }
        }
    }
}
